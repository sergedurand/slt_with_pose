# slt_with_pose

We use DOPE (https://github.com/naver/dope) and mediapipe (https://ai.googleblog.com/2020/12/mediapipe-holistic-simultaneous-face.html, https://google.github.io/mediapipe/solutions/holistic.html) to extract Human Pose estimation and experiment early fusion of the extracted features (keypoints coordinates) to the Sign Language Transformers model (https://github.com/neccam/slt)

## Quickstart

Load the notebook slt_pose.ipynb into colab and see instructions.

## Installation

See DOPE repo and SLT repo for their respective requirements. There was a bit of change from SLT requirements:
we used torchvision == 0.8.1, torch==1.7.0, tensorflow==2.4.1, pytorch-warmup==0.0.4 instead of warmup-scheduler
In addition you will need mpu and PyAV (pip install mpu, pip install av).


## Download data

DOPE features (~ 2GB): https://drive.google.com/file/d/1Y4aMsTHcN5cvtSO6J2mRNkQ8CvJ82Bug/view?usp=sharing

Mediapipe features (~ 11GB): https://drive.google.com/file/d/1jvtKCEY8T0u_eHmQ5puYNm0zsXniL54k/view?usp=sharing

copy to slt_with_pose/slt/data directory and extract (it takes a few minutes):
```sh
tar -xjf dope.bz2
tar -xjf mediapipe.bz2
```

The data is divided in folders train/dev/test each containing single files for each Phoenix14T video. 
See how it's loaded in the function build_batch in https://gitlab.com/sergedurand/slt_with_pose/-/blob/master/slt/signjoey/helpers.py

Original SLT features: 
move to slt_with_pose/slt/data directory and use the script:
```sh
sh download.sh
```

If you use other directory you'll have to change the config file accordingly (hopefully it will be enough).

## Usage


Run an experiment for a config file like this

```sh
python signjoey train configs/exemple.yaml
```

### config file details
Check the original paper for more details on the parameters. 

If you want to use features for a given part you have to put the size of the features in the pose section:

- hand_2d: 84
- hand_3d: 126
- body_2d: 14
- body_3d: 21
- face_2d: 168
- face_3d: 252
- vid_feat: 2048
- feature_size: 1024
- media_hand_2d: 84
- media_body_2d: 50
- media_face_2d: 936
- media_hand_3d: 126
- media_body_3d: 75
- media_face_3d: 936
- vid_norm: 1

As well as having extracted the features pickle files in the data directory (in subdirectories "dope" for the keypoints from dope, "dope_backbone" for the backbone hidden features, "mediapipe_2" for the mediapipe keypoints).

The vid_feat is for the DOPE backbone features and feature_size for the SLT hidden features. If vid_norm is set to 1 and you are using DOPE backbone features batch norm + ReLU will be applied to them (we didn't find noticeable difference).

Set to 0 for any feature you don't want to use.
Note that the mediapipe 3D coordinates are obtained from a lifting of the 2D coordinates, it is redundant to use them with 2D coordinates for the same part.

If you want to repeat experiments with different seeds, you can create the config files accordingly with an aggregate pickle filename where a dictionary containing the results will be saved.
See for exemple the zip files in configs folder (the path will need to be changed for all the config files..)

## Results

See the report for some discussion of the results. Overall there was no obvious improvement, even there is some hint that the human pose can be useful.
Detail results files are in the result directory, including some files on training statistics (scores before optimizing beams, epochs, final beam sizes, final best step...).
There is also a notebook that can be used to start exploring results e.g.

![](results/results.png)
