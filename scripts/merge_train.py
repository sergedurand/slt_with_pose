from pathlib import Path
import mpu

train_files = Path("train_pose_info/").glob("*.pickle")
res = dict()
cpt = 0
for path in train_files:
    vid_data = mpu.io.read(str(path))
    vid = path.stem
    if vid[:4] == "nms_":
        continue
    # dev : truncate to -3, train: truncate to -5, test: truncate to -4
    vid = vid[4:-5]
    vid = "train/" + vid
    for k, v in vid_data.items():
        if len(v[0]["hand"]) > 0:
            print(v[0]["hand"][0]["pose2d"].shape)
    res[vid] = vid_data[vid + ".mp4"]
    
mpu.io.write("ppi_dev.pickle",res)
