from pathlib import Path
import sys
sys.path.append("recvis_proj/")
from tqdm import tqdm
import numpy as np
from dope.dope import get_features
import torch
import cv2
import mediapipe as mp
mp_drawing = mp.solutions.drawing_utils
mp_holistic = mp.solutions.holistic
from slt.add_utils import load_batch_video
import mpu
import matplotlib.pyplot as plt
import torchvision

def normalize(arr):
    arr = arr.reshape(arr.shape[0],-1,3)
    arr -= np.mean(arr,axis=(0,1))
    arr /= np.std(arr,axis=(0,1))
    return arr.reshape(arr.shape[0],-1)

train_path = Path("/slt/videos/train/").glob("*.mp4")
train_path = list(train_path)
train_path.sort()
for i, path in enumerate(tqdm(train_path,position=0,leave=True)):
    name = "dev/" + path.stem
    if Path("drive/MyDrive/train_mediapipe/train_" + str(path.stem) + ".pickle").is_file():
        continue
    vid1 = load_batch_video(str(path))
    T = vid1.shape[0]
    vid_data = dict()
    vid_hand = list()
    vid_body = list()
    vid_face = list()
    holistic = mp_holistic.Holistic(static_image_mode=False,upper_body_only=True)
    video = list()
    for j in range(vid1.shape[0]):
        image = np.rint(vid1[j].permute(1,2,0).cpu().numpy() * 255).astype("uint8")
        image_hight, image_width, _ = image.shape
        results = holistic.process(image)
        # extracting hands:
        left_hand = list()
        if results.left_hand_landmarks is None:
            left_hand.append(np.zeros((21,3)))
        else:
            for ld in results.left_hand_landmarks.landmark:
                left_hand.append([ld.x,ld.y, ld.z])
        left_hand = np.array(left_hand)
        right_hand = list()
        if results.right_hand_landmarks is None:
            right_hand.append(np.zeros((21,3)))
        else:
            for rd in results.right_hand_landmarks.landmark:
                right_hand.append([rd.x, rd.y, rd.z])
        right_hand = np.array(right_hand)
        if len(right_hand.shape) > 2:
            right_hand = right_hand.squeeze(0)
        if len(left_hand.shape) > 2:
            left_hand = left_hand.squeeze(0)
        hand = np.concatenate([left_hand,right_hand])
        vid_hand.append(hand)

        # body
        if results.pose_landmarks is None:
            body = np.zeros((25,3))
        else:
            body = list()
            for bod in results.pose_landmarks.landmark:
                body.append([bod.x, bod.y, bod.z])
            body = np.array(body)
        vid_body.append(body)

        # face
        if results.face_landmarks is None:
            face = np.zeros((468,3))
        else:
            face = list()
            for fa in results.face_landmarks.landmark:
                face.append([fa.x, fa.y, fa.z])
            face = np.array(face)
        vid_face.append(face)
        ## Following is to save a video with the keypoints

        # #Draw pose, left and right hands, and face landmarks on the image.
        # annotated_image = image.copy()
        # mp_drawing.draw_landmarks(
        # annotated_image, results.face_landmarks, mp_holistic.FACE_CONNECTIONS)
        # mp_drawing.draw_landmarks(
        # annotated_image, results.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS)
        # mp_drawing.draw_landmarks(
        # annotated_image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS)
        # mp_drawing.draw_landmarks(
        # annotated_image, results.pose_landmarks, mp_holistic.POSE_CONNECTIONS)
        # video.append(torch.tensor(annotated_image))
    holistic.close()
    # torchvision.io.write_video(str(train_path[i].stem) + ".mp4",torch.stack(video),fps=25)
    vid_body = np.array(vid_body).reshape(T,-1)
    vid_face = np.array(vid_face).reshape(T,-1)
    vid_hand = np.array(vid_hand).reshape(T,-1)
    vid_data["hand"] = vid_hand
    vid_data["face"] = vid_face
    vid_data["body"] = vid_body
    mpu.io.write("drive/MyDrive/train_mediapipe/train_" +str(path.stem) + ".pickle", vid_data)
