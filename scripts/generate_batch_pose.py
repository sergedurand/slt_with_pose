import torch
import numpy as np
from time import time
import mpu

ppi = mpu.io.read("ppi_dev.pickle")
poses = ["2d","3d"]
parts = ["hand","body","face"]
batches = dict()
for vid, data in ppi.items():
    batch = dict()
    for part in parts:
        for pose in poses:
            k_name = part + "_" + pose
            L = list()
            for i in range(len(data)):
                if len(data[i][part]) == 1:
                    pose_estim = data[i][part][0]["pose"+pose]
                    if pose == "2d": 
                        pose_estim[:,0] /= 210
                        pose_estim[:,1] /= 260
                    if part == "hand":
                        print(data[i][part][0]["pose2d"].shape)
                        print(data[i][part][0]["pose3d"].shape)
                else:
                    if part == "hand":
                        s1 = 42
                    elif part == "body":
                        s1 = 13
                    else:
                        s1 = 84
                    if pose == "2d":
                        s2 = 2
                    else:
                        s2 = 3
                    pose_estim = np.zeros(shape=(s1,s2))
                if part == "body":
                    # keeping upper body only
                    pose_estim = pose_estim[6:]
                L.append(pose_estim.flatten())
            batch[k_name] = np.array(L)
     mpu.io.write("slt/data/dope/" +vid.split(".")[0] + ".pickle",batch)
    

