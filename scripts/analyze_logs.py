from pathlib import Path
import mpu
import pandas as pd

res = dict()
pd.set_option('float_format', '{:.2f}'.format)

paths = Path("./").glob("slt_FBH3D_256_1024_*/train.log")
for path in paths:    
    with path.open("r") as f:
        lines = f.readlines()
        for line in lines:
            if "Best validation result at step" in line:
                line = line.split()
                i = line.index("step")
                step = line[i+1].rstrip(":")
            if "cfg.training.random_seed" in line:
                line = line.split()
                seed = line[-1]

        for i in range(len(lines)):
            line = lines[i]
            if "{}: duration:".format(step) in line:
                line = line.split()
                epoch = line[line.index("epoch")+1].rstrip(",")
                wer = lines[i+4].split()[1]
                bleu = lines[i+5].split()[1]
                
            if "[DEV] partition [Recognition & Translation] results:" in line:
                CTC_beam = lines[i+1].split()[-1]
                trans_beam = lines[i+2].split()[-4]
                alpha = lines[i+2].split()[-1]
#                 print(CTC_beam)
#                 print(trans_beam)
#                 print(alpha)
        res[int(seed)] = {"wer_base": float(wer), "bleu_base": float(bleu),"epoch": int(epoch),"step": int(step),
                         "CTC_bream": int(CTC_beam),"trans_beam": int(trans_beam), "alpha": int(alpha)}
        
        
# paths = Path("./").glob("slt_FBH2D_*/train.log")
# for path in paths:    
#     with path.open("r") as f:
#         lines = f.readlines()
#         for line in lines:
#             if "Best validation result at step" in line:
#                 line = line.split()
#                 i = line.index("step")
#                 step = line[i+1].rstrip(":")
#             if "cfg.training.random_seed" in line:
#                 line = line.split()
#                 seed = line[-1]

#         for i in range(len(lines)):
#             line = lines[i]
#             if "{}: duration:".format(step) in line:
#                 line = line.split()
#                 epoch = line[line.index("epoch")+1].rstrip(",")
#                 wer = lines[i+4].split()[1]
#                 bleu = lines[i+5].split()[1]
#         res[int(seed)] = {"wer_base": float(wer), "bleu_base": float(bleu),"epoch": int(epoch),"step": int(step)}
        
print(res.keys())
df = pd.DataFrame.from_dict(res,"index")
print(df.describe())

# mpu.io.write("DOPE_Hidden_512_trainstat.pickle",res)