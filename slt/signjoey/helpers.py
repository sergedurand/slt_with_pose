# coding: utf-8
"""
Collection of helper functions
"""
import copy
import errno
import glob
import logging
import os
import os.path
import random
import shutil
from logging import Logger
from pathlib import Path
from sys import platform
from typing import Callable, Optional

import mpu
import numpy as np
import torch
import yaml
from torch import nn, Tensor
from torchtext.data import Dataset
import torch.nn.functional as F

from slt.signjoey.vocabulary import GlossVocabulary, TextVocabulary


def make_model_dir(model_dir: str, overwrite: bool = False) -> str:
    """
    Create a new directory for the model.

    :param model_dir: path to model directory
    :param overwrite: whether to overwrite an existing directory
    :return: path to model directory
    """
    if os.path.isdir(model_dir):
        if not overwrite:
            raise FileExistsError("Model directory exists and overwriting is disabled.")
        # delete previous directory to start with empty dir again
        shutil.rmtree(model_dir)
    os.makedirs(model_dir)
    return model_dir


def make_logger(model_dir: str, log_file: str = "train.log") -> Logger:
    """
    Create a logger for logging the training process.

    :param model_dir: path to logging directory
    :param log_file: path to logging file
    :return: logger object
    """
    logger = logging.getLogger(__name__)
    if not logger.handlers:
        logger.setLevel(level=logging.DEBUG)
        fh = logging.FileHandler("{}/{}".format(model_dir, log_file))
        fh.setLevel(level=logging.DEBUG)
        logger.addHandler(fh)
        formatter = logging.Formatter("%(asctime)s %(message)s")
        fh.setFormatter(formatter)
        if platform == "linux":
            sh = logging.StreamHandler()
            sh.setLevel(logging.INFO)
            sh.setFormatter(formatter)
            logging.getLogger("").addHandler(sh)
        logger.info("Hello! This is Joey-NMT.")
        return logger


def log_cfg(cfg: dict, logger: Logger, prefix: str = "cfg"):
    """
    Write configuration to log.

    :param cfg: configuration to log
    :param logger: logger that defines where log is written to
    :param prefix: prefix for logging
    """
    for k, v in cfg.items():
        if isinstance(v, dict):
            p = ".".join([prefix, k])
            log_cfg(v, logger, prefix=p)
        else:
            p = ".".join([prefix, k])
            logger.info("{:34s} : {}".format(p, v))


def clones(module: nn.Module, n: int) -> nn.ModuleList:
    """
    Produce N identical layers. Transformer helper function.

    :param module: the module to clone
    :param n: clone this many times
    :return cloned modules
    """
    return nn.ModuleList([copy.deepcopy(module) for _ in range(n)])


def subsequent_mask(size: int) -> Tensor:
    """
    Mask out subsequent positions (to prevent attending to future positions)
    Transformer helper function.

    :param size: size of mask (2nd and 3rd dim)
    :return: Tensor with 0s and 1s of shape (1, size, size)
    """
    mask = np.triu(np.ones((1, size, size)), k=1).astype("uint8")
    return torch.from_numpy(mask) == 0


def set_seed(seed: int):
    """
    Set the random seed for modules torch, numpy and random.

    :param seed: random seed
    """
    torch.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)


def log_data_info(
        train_data: Dataset,
        valid_data: Dataset,
        test_data: Dataset,
        gls_vocab: GlossVocabulary,
        txt_vocab: TextVocabulary,
        logging_function: Callable[[str], None],
):
    """
    Log statistics of data and vocabulary.

    :param train_data:
    :param valid_data:
    :param test_data:
    :param gls_vocab:
    :param txt_vocab:
    :param logging_function:
    """
    logging_function(
        "Data set sizes: \n\ttrain {:d},\n\tvalid {:d},\n\ttest {:d}".format(
            len(train_data),
            len(valid_data),
            len(test_data) if test_data is not None else 0,
        )
    )

    logging_function(
        "First training example:\n\t[GLS] {}\n\t[TXT] {}".format(
            " ".join(vars(train_data[0])["gls"]), " ".join(vars(train_data[0])["txt"])
        )
    )

    logging_function(
        "First 10 words (gls): {}".format(
            " ".join("(%d) %s" % (i, t) for i, t in enumerate(gls_vocab.itos[:10]))
        )
    )
    logging_function(
        "First 10 words (txt): {}".format(
            " ".join("(%d) %s" % (i, t) for i, t in enumerate(txt_vocab.itos[:10]))
        )
    )

    logging_function("Number of unique glosses (types): {}".format(len(gls_vocab)))
    logging_function("Number of unique words (types): {}".format(len(txt_vocab)))


def load_config(path="configs/default.yaml") -> dict:
    """
    Loads and parses a YAML configuration file.

    :param path: path to YAML configuration file
    :return: configuration dictionary
    """
    with open(path, "r", encoding="utf-8") as ymlfile:
        cfg = yaml.safe_load(ymlfile)
    return cfg


def bpe_postprocess(string) -> str:
    """
    Post-processor for BPE output. Recombines BPE-split tokens.

    :param string:
    :return: post-processed string
    """
    return string.replace("@@ ", "")


def get_latest_checkpoint(ckpt_dir: str) -> Optional[str]:
    """
    Returns the latest checkpoint (by time) from the given directory.
    If there is no checkpoint in this directory, returns None

    :param ckpt_dir:
    :return: latest checkpoint file
    """
    list_of_files = glob.glob("{}/*.ckpt".format(ckpt_dir))
    latest_checkpoint = None
    if list_of_files:
        latest_checkpoint = max(list_of_files, key=os.path.getctime)
    return latest_checkpoint


def load_checkpoint(path: str, use_cuda: bool = True) -> dict:
    """
    Load model from saved checkpoint.

    :param path: path to checkpoint
    :param use_cuda: using cuda or not
    :return: checkpoint (dict)
    """
    assert os.path.isfile(path), "Checkpoint %s not found" % path
    checkpoint = torch.load(path, map_location="cuda" if use_cuda else "cpu")
    return checkpoint


# from onmt
def tile(x: Tensor, count: int, dim=0) -> Tensor:
    """
    Tiles x on dimension dim count times. From OpenNMT. Used for beam search.

    :param x: tensor to tile
    :param count: number of tiles
    :param dim: dimension along which the tensor is tiled
    :return: tiled tensor
    """
    if isinstance(x, tuple):
        h, c = x
        return tile(h, count, dim=dim), tile(c, count, dim=dim)

    perm = list(range(len(x.size())))
    if dim != 0:
        perm[0], perm[dim] = perm[dim], perm[0]
        x = x.permute(perm).contiguous()
    out_size = list(x.size())
    out_size[0] *= count
    batch = x.size(0)
    x = (
        x.view(batch, -1)
            .transpose(0, 1)
            .repeat(count, 1)
            .transpose(0, 1)
            .contiguous()
            .view(*out_size)
    )
    if dim != 0:
        x = x.permute(perm).contiguous()
    return x


def freeze_params(module: nn.Module):
    """
    Freeze the parameters of this module,
    i.e. do not update them during training

    :param module: freeze parameters of this module
    """
    for _, p in module.named_parameters():
        p.requires_grad = False


def symlink_update(target, link_name):
    try:
        os.symlink(target, link_name)
    except FileExistsError as e:
        if e.errno == errno.EEXIST:
            os.remove(link_name)
            os.symlink(target, link_name)
        else:
            raise e


def build_batch(batch, use_cfg):
    """
    Takes a batch from Phoenix and replaces the features in batch.sgn according to the config file
    :param batch:
    :param use_cfg:
    :return:
    """
    video_names = batch.sequence
    use_cnn = False
    use_vid_feat = False
    use_dope_pose = False
    use_media_pose = False
    max_len = int(batch.sgn[1].max().item())
    for k, size in use_cfg.items():
        if k.startswith("media") and size != 0:
            use_media_pose = True
        if (k.startswith("hand") or k.startswith("body") or k.startswith("face")) and size != 0:
            use_dope_pose = True
        if k == "vid_feat" and size != 0:
            use_vid_feat = True
        if k == "feature_size" and size != 0:
            use_cnn = True
        if k == "vid_norm" != 0:
            normalizer = torch.nn.Sequential(torch.nn.BatchNorm1d(num_features=use_cfg["vid_feat"], affine=False),
                                             torch.nn.ReLU())
            if torch.cuda.is_available():
                normalizer = normalizer.cuda()
    if not(use_vid_feat or use_dope_pose or use_media_pose) and use_cnn:
        return batch
    new_sgn = list()
    i = 0
    prefix = Path(__file__).parent.parent.resolve()
    for name in video_names:
        cur_sgn = torch.Tensor()
        if use_media_pose:
            path = str(prefix) + "/data/" + "mediapipe_2/" + name + ".pickle"
            data = mpu.io.read(path)
            media_data = torch.Tensor()
            for k, v in data.items():
                for dim in ["2d","3d"]:
                    key = "media_" + k + "_"  + dim
                    if key in use_cfg.keys() and use_cfg[key] != 0:
                        if dim == "2d":
                            _data = torch.tensor(v[:,:use_cfg[key]])
                        else:
                            _data = torch.tensor(v)
                        media_data = torch.cat((media_data, _data), -1)
            cur_sgn = torch.cat((cur_sgn, media_data), -1)
        if use_dope_pose:
            path = str(prefix) + "/data/" + "dope/" + name + ".pickle"
            data = mpu.io.read(path)
            pose_data = torch.Tensor()
            for k, v in data.items():
                if use_cfg[k] != 0:
#                     if k == "body_2d" and v.shape[-1] != 14:
#                         v = v.reshape(v.shape[0],-1,2)
#                         v = v[:,6:,:].reshape(v.shape[0],-1)
#                     if k == "body_3d" and v.shape[-1] != 21:
#                         v = v.reshape(v.shape[0],-1,3)
#                         v = v[:,6:,:].reshape(v.shape[0],-1)
                    pose_data = torch.cat((pose_data, torch.tensor(v)), -1)
            cur_sgn = torch.cat((cur_sgn, pose_data), -1)

        if use_vid_feat:
            path = str(prefix) + "/data/" + "dope_backbone/" + name + ".pickle"
            data = mpu.io.read(path)
            data = torch.tensor(data)
            if torch.cuda.is_available():
                data = data.cuda()
            if use_cfg["vid_norm"] != 0:
                with torch.no_grad():
                    data = normalizer(data).cpu()
            cur_sgn = torch.cat((cur_sgn, data), - 1)
        new_sgn.append(cur_sgn)
    new_sgn = torch.nn.utils.rnn.pad_sequence(new_sgn, batch_first=True)
    if use_cnn:
        batch.sgn = (torch.cat((batch.sgn[0], new_sgn), -1).float(), batch.sgn[1])
    else:
        batch.sgn = (new_sgn.float() + 1e-8, batch.sgn[1])
    return batch

def get_feature_size(cfg):
    feature_size = 0
    for k, size in cfg.items():
        if k in ["path", "pose_norm", "vid_norm"]: # those are not feature sizes
            continue
        feature_size += size
    return feature_size
