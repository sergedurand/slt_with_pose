import mpu
from pathlib import Path
import gzip
import pickle
from time import time
import csv

def load_dataset_file(filename):
    with gzip.open(filename, "rb") as f:
        loaded_object = pickle.load(f)
        return loaded_object
    
# start = time()
# data_paths = Path(__file__).resolve().parent.parent.parent.glob("train*.pickle")
# data = dict()
# for path in data_paths:
#     _data = mpu.io.read(str(path))
#     data = {**data, **_data}
# elapsed = time() - start
# print("Loading vid features with mpu = {:.2f} s".format(elapsed))

start = time()
data_b = load_dataset_file("/tempory/recvis_proj/slt/data/phoenix14t.pami0.test")
elapsed = time() - start
print("Loading vid features with gzip = {:.2f} s".format(elapsed))
start = time() 
mpu.io.write("/tempory/recvis_proj/phoenix.14t.parmi0.test.pickle",data_b)
elapsed = time() - start
print("Loading vid features with mpu = {:.2f} s".format(elapsed))
# start = time()
# data_c = mpu.io.read("/tempory/recvis_proj/slt/data/phoenix14t.pami0.dev")
# elapsed = time() - start
# print("Loading vid features with mpu = {:.2f".format(elapsed))

# print(type(data_c))
# print(len(data_c))