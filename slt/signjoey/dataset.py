# coding: utf-8
"""
Data module
"""
import gzip
import pickle
from typing import Tuple
from pathlib import Path

import mpu

this_dir = Path(__file__).parent.resolve()
import sys

sys.path.append(str(this_dir.parent.parent))  # project dir is parent of parent of current file
import torch
import torchvision
from torchtext import data
from torchtext.data import Field, RawField

from slt.add_utils import load_pose_estim, load_batch_video

from dope.dope import get_features


def load_dataset_file(filename):
    with gzip.open(filename, "rb") as f:
        loaded_object = pickle.load(f)
        return loaded_object


def load_vid_features(name="train"):
    data_paths = Path(__file__).resolve().parent.parent.parent.glob(name + "*.pickle")
    data = dict()
    for path in data_paths:
        _data = mpu.io.read(str(path))
        data = {**data, **_data}
    return data


class SignTranslationDataset(data.Dataset):
    """Defines a dataset for machine translation."""

    @staticmethod
    def sort_key(ex):
        return data.interleave_keys(len(ex.sgn), len(ex.txt))

    def __init__(
            self,
            path: str,
            fields: Tuple[RawField, RawField, Field, Field, Field],
            **kwargs
    ):
        """Create a SignTranslationDataset given paths and fields.

        Arguments:
            path: Common prefix of paths to the data files for both languages.
            exts: A tuple containing the extension to path for each language.
            fields: A tuple containing the fields that will be used for data
                in each language.
            Remaining keyword arguments: Passed to the constructor of
                data.Dataset.
        """
        if not isinstance(fields[0], (tuple, list)):
            fields = [
                ("sequence", fields[0]),
                ("signer", fields[1]),
                ("sgn", fields[2]),
                ("gls", fields[3]),
                ("txt", fields[4]),
            ]

        if not isinstance(path, list):
            path = [path]

        # use_vid_feat : we use hidden features extracted from pretrained DOPE backbone
        # use_cnn_feat : we use hidden features extracted from pretrained CNN (original slt implementation)
        # use_pose_feat : we use pose estimation data extracted from DOPE

        # normalizing the additional feature extracted from resnet with Batch Norm + ReLU (cf slt paper)
        # if use_pose_cfg["vid_feat"] != 0:
        #     use_vid_feat = True
        # if use_pose_cfg["vid_norm"] != 0:
        #     print("Normalizing Dope extracted features")
        #     normalizer = torch.nn.Sequential(torch.nn.BatchNorm1d(num_features=use_pose_cfg["vid_feat"], affine=False),
        #                                      torch.nn.ReLU())
        #     if torch.cuda.is_available():
        #         normalizer = normalizer.cuda()
        # use_cnn_feat = True
        # if use_pose_cfg["feature_size"] == 0:
        #     use_cnn_feat = False
        samples = {}
        # checking if we're using pose estimation
        # if we are we must skip some samples as we don't have pose estim for all videos
        # use_pose = False
        # use_media = False
        # using = list()
        # for k, size in use_pose_cfg.items():
        #     if k == "path":
        #         continue
        #     if size != 0:
        #         use_pose = True
        #         if k.startswith("media"):
        #             use_media = True
        #         using.append(k)
        for annotation_file in path:
            tmp = load_dataset_file(annotation_file)
            mode = annotation_file.split(".")[-1]
            if mode not in ["train", "test", "dev"]:
                raise FileNotFoundError(mode)
            # we load pose data even if we don't use it, to skip the videos
            # that we didn't extract pose from, so we can use same train set
            # for all experiences
            #             if use_pose_cfg["pose_norm"] == 0:
            #                 suffix = "_batches.pickle"
            #             elif use_pose_cfg["pose_norm"] ==1:
            #                 suffix = "_batches_norm.pickle"
            #             else:
            # suffix = "_batches_norm_no_leg.pickle"
            # pose_data = mpu.io.read(use_pose_cfg["path"] + mode + suffix)
            # if use_media:
            #     media_data = mpu.io.read(str(this_dir.parent.resolve()) + "/data/mediapipe/" + mode + ".pickle")
            # vid_features = None
            # if use_vid_feat:
            #     vid_features = load_vid_features(mode)
            for s in tmp:
                seq_id = s["name"]
                # if seq_id not in pose_data.keys():
                #     continue
                # if not use_cnn_feat:  # not using the original features:
                #     s["sign"] = torch.Tensor()  # empty tensor
                # if vid_features is not None:
                #     vid_feat = torch.tensor(vid_features[seq_id])
                #     if use_pose_cfg["vid_norm"] != 0:
                #         if torch.cuda.is_available():
                #             vid_feat = vid_feat.cuda()
                #         with torch.no_grad():
                #             vid_feat = normalizer(vid_feat).cpu()
                #     s["sign"] = torch.cat((s["sign"], vid_feat), dim=-1)
                # if use_pose:
                #     batch = pose_data[seq_id]
                #     for part, pose_estim in batch.items():
                #         if use_pose_cfg[part] != 0:
                #             s["sign"] = torch.cat((s["sign"], torch.tensor(pose_estim)), dim=-1)
                # if use_media:
                #     batch = media_data[seq_id]
                #     for part, pose_estim in batch.items():
                #         if use_pose_cfg["media_" + part] != 0:
                #             s["sign"] = torch.cat((s["sign"], torch.tensor(pose_estim)), dim=-1)
                if seq_id in samples:
                    assert samples[seq_id]["name"] == s["name"]
                    assert samples[seq_id]["signer"] == s["signer"]
                    assert samples[seq_id]["gloss"] == s["gloss"]
                    assert samples[seq_id]["text"] == s["text"]
                    samples[seq_id]["sign"] = torch.cat(
                        [samples[seq_id]["sign"], s["sign"]], axis=-1
                    )

                else:
                    samples[seq_id] = {
                        "name": s["name"],
                        "signer": s["signer"],
                        "gloss": s["gloss"],
                        "text": s["text"],
                        "sign": s["sign"],
                    }

        examples = []
        for s in samples:
            sample = samples[s]
            #             print("Sample size ",sample["sign"].shape)
            examples.append(
                data.Example.fromlist(
                    [
                        sample["name"],
                        sample["signer"],
                        # This is for numerical stability
                        sample["sign"] + 1e-8,
                        sample["gloss"].strip(),
                        sample["text"].strip(),
                    ],
                    fields,
                )
            )
        print(f"Size of the dataset = {len(examples)}")
        super().__init__(examples, fields, **kwargs)

